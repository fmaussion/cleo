.. -*- rst -*- -*- restructuredtext -*-
.. This file should be written using restructured text conventions

====
Cleo
====
.. image:: https://coveralls.io/repos/fmaussion/cleo/badge.svg?branch=master&service=bitbucket
  :target: https://coveralls.io/bitbucket/fmaussion/cleo?branch=master


Cleo is a `cat <https://drive.google.com/file/d/0B-0AsTwFw61uRnZZY1l4cjU2b3M
/view?usp=sharing>`_, but Cleo is also a small library to do some geoscientific
data plotting.


Installation
------------

Cleo needs the libraries numpy, scipy, pyproj, matplotlib and netCDF4. Cleo
builds upon the companion project `Salem <https://bitbucket
.org/fmaussion/salem>`_.
After installing the required packages, a simple `pip install` should be
enough::

    $ pip install git+https://bitbucket.org/fmaussion/cleo.git#egg=Cleo


Classes
-------

**DataLevels**
    Associate the right color to the data according to user specifications.
    It is useful because it prevents any mismatch between plot and colorbar
    by managing both concepts together and not separated as matplotlib does.

**Map**
    A cartographic representation of georeferenced 2D data. Handles country
    borders, all kinds of geometries, topographical shading, and map
    projections.

About
-----

:License:
    GNU GPLv3

:Author:
    Fabien Maussion - fabien.maussion@uibk.ac.at
