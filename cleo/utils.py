from __future__ import division
import os
import pickle
import numpy as np
import pyproj
import salem
from descartes.patch import PolygonPatch

# Joblib
from joblib import Memory
memory = Memory(cachedir=salem.cache_dir, verbose=0)


@memory.cache(ignore=['grid'])
def _memory_transform(shape_cpath, grid=None, grid_str=None):
    shape = salem.utils.read_shapefile(shape_cpath, cached=True)
    if grid is not None:
        e = grid.extent_in_crs(crs=shape.crs)
        p = np.nonzero(~((shape['min_x'] > e[1]) |
                         (shape['max_x'] < e[0]) |
                         (shape['min_y'] > e[3]) |
                         (shape['max_y'] < e[2])))
        shape = shape.iloc[p]

    shape = salem.transform_geopandas(shape, to_crs=grid, inplace=True)
    return shape, shape.crs


def transform_shape_to_grid(shape_path, grid=None):

    # ensure it is a cached pickle (copy code smell)
    shape_cpath = salem.utils.cached_path(shape_path)
    if not os.path.exists(shape_cpath):
        out = salem.utils.read_shapefile(shape_path, cached=False)
        pick = dict(gpd=out, crs=out.crs)
        with open(shape_cpath, 'wb') as f:
            pickle.dump(pick, f)
    gstr = str(grid)
    #TODO: remove this when new geopandas is out
    out, crs = _memory_transform(shape_cpath, grid=grid, grid_str=gstr)
    out.crs = crs
    return out


def make_local_mercator_grid(center_ll=None, extent=None, nx=None, ny=None,
                             order='ll'):
    """Local mercator map centered on a specified point."""

    # Make a local proj
    lon, lat = center_ll
    proj_params = dict(proj='tmerc', lat_0=0., lon_0=lon,
                       k=0.9996, x_0=0, y_0=0, datum='WGS84')
    projloc = pyproj.Proj(proj_params)

    # Define a spatial resolution
    xx = extent[0]
    yy = extent[1]
    if ny is None and nx is None:
        ny = 600
        nx = ny * xx / yy
    else:
        if nx is not None:
            ny = nx * yy / xx
        if ny is not None:
            nx = ny * xx / yy
    nx = np.rint(nx)
    ny = np.rint(ny)

    e, n = pyproj.transform(salem.wgs84, projloc, lon, lat)

    if order=='ul':
        corner = (-xx / 2. + e, yy / 2. + n)
        dxdy = (xx / nx, - yy / ny)
    else:
        corner = (-xx / 2. + e, -yy / 2. + n)
        dxdy = (xx / nx, yy / ny)

    return salem.Grid(proj=projloc, corner=corner, nxny=(nx, ny), dxdy=dxdy,
                      pixel_ref='corner')


def plot_polygon(ax, poly, edgecolor='black', **kwargs):
    """ Plot a single Polygon geometry """

    a = np.asarray(poly.exterior)
    # without Descartes, we could make a Patch of exterior
    ax.add_patch(PolygonPatch(poly, **kwargs))
    ax.plot(a[:, 0], a[:, 1], color=edgecolor)
    for p in poly.interiors:
        x, y = zip(*p.coords)
        ax.plot(x, y, color=edgecolor)